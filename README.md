# 01. About Me

- [Hallo zeggen](https://projects.raspberrypi.org/nl-NL/projects/about-me/2)
- [Uitdaging: waar denk je aan?](https://projects.raspberrypi.org/nl-NL/projects/about-me/3)
- [ASCII-kunst](https://projects.raspberrypi.org/nl-NL/projects/about-me/4)
- [Uitdaging: over jezelf](https://projects.raspberrypi.org/nl-NL/projects/about-me/5)
- [Het jaar 2025](https://projects.raspberrypi.org/nl-NL/projects/about-me/6)
- [Uitdaging: je leeftijd in hondenjaren](https://projects.raspberrypi.org/nl-NL/projects/about-me/7)
- [Uitdaging: rekenen met tekst](https://projects.raspberrypi.org/nl-NL/projects/about-me/8)