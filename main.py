#!/bin/python3

# Als je het interactief wil maken moet je deze code activeren door de '#' ervoor te verwijderen
# geboren = input("Wat is je geboortejaar?")
# geboren = int(geboren)

# En de lijn hieronder in commentaar zetten door een '#' er voor te zetten
geboren = 1982
leeftijd = 2025 - geboren
print('In \'25 ben je ', leeftijd, ' jaar')

hondenjaren = leeftijd * 7
print ('Als je een 🐶 hond zou zijn zou je', hondenjaren, 'jaar oud zijn')

print('Hier is een sjaal: ')
print('~#' * 10)
print('#~' * 10)

print('Hier is een golf:')
print('/\ ' * 10)
print('  \/' * 10)